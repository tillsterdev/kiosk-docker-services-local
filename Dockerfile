FROM tomcat:8.5-jdk8-openjdk-slim
MAINTAINER William Dickerson <wdickerson@tillster.com>

USER root

COPY config/server.xml /usr/local/tomcat/conf/server.xml
COPY config/tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml

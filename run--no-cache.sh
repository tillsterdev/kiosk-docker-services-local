#!/usr/bin/env bash

echo ''
echo '======'
echo '[INFO] This script will now run/refresh the build w/o Cache & force re-creating the Containers'
echo '======'
echo ''
echo '======'
echo '[INFO] Building Kiosk Local Development Environment w/o Cache'
echo '======'
echo ''
docker-compose build --no-cache

echo ''
echo ''
echo '======'
echo '[INFO] Forcibly Initializing Kiosk Local Development Environment'
echo '======'
echo ''
docker-compose up -d --force-recreate

echo ''
#!/usr/bin/env bash

docker-compose down
docker volume prune
docker system prune -a
## What's this?

* This is the dir that simulates `C:/tillster` on a Kiosk
* Therefore, simply place files here that are required for modules to initialize, e.g. tillster/bin/config
* These will be picked up during Tomcat Docker Container init, which will map it directly into the container at /tillster

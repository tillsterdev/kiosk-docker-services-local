## Required Software

* Python
    * https://www.python.org/downloads/release/python-361/
    * Choose to add to PATH during Install & Disable PATH length limit upon completion
* Docker
    * https://www.docker.com/products/docker-desktop
        * Windows -- When installing, put checkmark into installing required Windows Subsystem for Linux (WSL)
            * More Info
                * https://docs.microsoft.com/en-us/windows/wsl/install-win10
                * https://docs.docker.com/docker-for-windows/wsl/
    * Where to get new Images, or update Versions
        * https://hub.docker.com
* Windows -- Git Bash 
        * Usage: To run the .sh files
        * https://gitforwindows.org/

### Installed Packages

* The following packages are installed into the container:
    * Redis
    * MySQL
    * Tomcat
        * w/ Manager

----

## First Run

### Get & Assemble the Application

* First, make sure you have Docker running
* Then, run the following in your terminal, or Git Bash for Windows Users
    * ```sh run--no-cache.sh```
    * WARNING:
        * If you incorrectly do ```sh run.sh``` some things may not work, e.g. Tomcat Manager login
    * What does it do?
        * This will
           * Follow the Dockerfile & docker-compose.yml to get the required images
           * Copy files in ```/config``` to the Container
           * Initialize the Application

#### Common Errors

* If you see several stacktraces w/ errors, including a line:
    * ```pywintypes.error: (2, 'CreateFile', 'The system cannot find the file specified.')```
    * Make sure that you have Docker fully up and running.


### How do I know it was Successful?

#### Easy Way
* Easy way is to just navigate to http://localhost:8085
    * You should see here a simple Tomcat Version and success msg

#### Complete Way
* Console output should show
    * Downloading of missing images the App. requires
    * Complete a series of steps to prepare the App.
        * E.g. ```Step 2/5```
    * Initialize the Containers that comprise the App.
        * E.g. ```Creating kiosk-tomcat ... done```
    * Once the initialization is completed
        * The application & containers should appear in your Docker Desktop application
            * Application named ```docker-kiosk-services-local```
            * Clicking this should expand it and display the individual Containers
            * All of these should be labelled ```RUNNING```


### It works!... What next?

#### Run Your Own Webapps

* Place your own webapps into ```/webapps```
    * Given Tomcat is set to auto-unpack & auto-deploy
        * See server.xml in ```/config```
    * Therefore the easy way is to just paste/move the webapp into the dir & Tomcat will handle the rest
        * See ```kiosk-tomcat``` in Docker Desktop for easy start/stop/restart tools & console

#### Monitor the Application

* Stack includes the minimal Tomcat Manager by default
    * https://tomcat.apache.org/tomcat-7.0-doc/manager-howto.html
    * Note:
        * If you're already familiar w/ this, mind that it has been pruned somewhat
            * Mostly to cut out extra documentation/handling
    * How do I get there?
        * When the Application is running, you can either
            * Navigate to http://localhost:8085
            * Or use Docker Desktop to
                * Hover over the Tomcat container in our Application
                * Then click the square w/ an arrow named ```Open in Browser```
    * What do I do with it?
        * If you're unfamiliar, and on the main page from the last step
            * ```Server Status```
                * Use this to monitor resource consumption, versions, processing, transactions, etc.
                * You can also click the link to ```Complete Server Status``` to get a larger amount of info
            * ```Manager App```
                * Use this to directly manage webapps within the Tomcat instance
                    * E.g. to stop/start/reload/undeploy indiv. webapps
                * Default UID: `root`
                * Default password: `root`


## Helper Scripts

* Easy Running/Stopping (After First Run Script has completed)
    * ```sh run.sh```
    * ```sh stop.sh```
* Easy Hard Refresh
    * ```sh run--no-cahe```
        * This will rebuild the container from scratch, doing everything but re-downloading images
    * ```sh destroy-all_docker_data.sh```
        * The hardest refresh, which will 
            * Stop all Applications/Containers & destroy them completely
                * ALL OF YOUR APPLICATIONS, NOT JUST THIS ONE! :)
            * Destroy all images/content, which will therefore download on next run
        * Used when 
            * We have changed the Application heavily and want to completely rebuild
            * We've done a lot of prototyping and want to clean the workspace completely
        
